from django.urls import path
from .views import show_sales_record, list_salespersons, show_salespersons, show_customer, list_customers, sales_records, list_automobiles, sales_history


urlpatterns = [
    path("salespersons/", list_salespersons, name="list_salespersons"),
    path("salespersons/<int:pk>/", show_salespersons, name="show_salespersons"),
    path("customers/", list_customers, name="list_customers" ),
    path("customers/<int:pk>/", show_customer, name="show_customer"),
    path("automobiles/", list_automobiles, name="list_automobiles" ),
    path("sales/", sales_records, name="sales_records"),
    path("sales/<int:pk>", show_sales_record, name="show_sales_record"),
    path("salespersons/<int:employee_id>/sales/", sales_records, name="person_sale_history"),
]
