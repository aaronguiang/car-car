from django.contrib import admin
from .models import AutomobileVO, Customer, Salesperson, SalesRecord


admin.site.register(AutomobileVO)
admin.site.register(Customer)
admin.site.register(Salesperson)
admin.site.register(SalesRecord)
