from django.shortcuts import render, get_object_or_404
from .models import AutomobileVO, Customer, Salesperson, SalesRecord
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import SalespersonEncoder, SalesRecordEncoder, CustomerEncoder, AutomobileVOEncoder


@require_http_methods(["GET", "POST"])
def list_salespersons(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"Message": "Error creating salesperson"},
                status=400
            )
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def show_salespersons(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salesperson,
                encoder =SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "No employee match"},
                status=404)
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder =SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "No employee match"},
                status=404)
            return response
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.get(id=pk)
            props = ["name", "employee_id"]
            for prop in props:
                if prop in content:
                    setattr(salesperson, prop, content[prop])
            salesperson.save()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
            {"message": "No employee match"},
            status=404)
            return response


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({
            "customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"Message": "Error creating customer"},
                status=400
            )
            return response


@require_http_methods(["GET", "PUT", "DELETE"])
def show_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "No customer match"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder =CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "No customer match"})
            response.status_code = 404
            return response

    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)
            props = ["name", "address", "phone"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "No customer match"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse({
            "automobiles": automobiles},
            encoder = AutomobileVOEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def sales_records(request, employee_id=None):
    if request.method == "GET":
        if employee_id == None:
            salesrecords = SalesRecord.objects.all()
        else:
            salesrecords = SalesRecord.objects.filter(employee_id=employee_id)
        return JsonResponse(
            {"salesrecords": salesrecords},
            encoder=SalesRecordEncoder,
            safe=False
        )

    else:
        content = json.loads(request.body)
        try:
            vin = content['automobile']
            salesvin = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = salesvin

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "No Automobile"},
                status=400,
            )
        try:
            employee_number = content['salesperson']
            employee = Salesperson.objects.get(id=employee_number)
            content["salesperson"] = employee

        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "No salesperson"},
                status=400,
            )
        try:
            customer_number = content['customer']
            customer = Customer.objects.get(id=customer_number)
            content["customer"] = customer

        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "No customer"},
                status=400,
            )
        salesrecord = SalesRecord.objects.create(**content)
        return JsonResponse(
                salesrecord,
                encoder=SalesRecordEncoder,
                safe=False,
            )


@require_http_methods(["GET"])
def sales_history(request, employee_id):
    if request.method == "GET":
        salesperson_id = Salesperson.objects.get(id=employee_id)
        salesrecords = SalesRecord.objects.filter(salesperson_id=salesperson_id)
        return JsonResponse(
            {"salesrecords": salesrecords},
            encoder=SalesRecordEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def show_sales_record(request, pk):
    if request.method == "GET":
        try:
            sales_record = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "No SALE"})
            response.status_code = 404
            return response
    else:
        try:
            sale = SalesRecord.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder =SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "No sale match"})
            response.status_code = 404
            return response
