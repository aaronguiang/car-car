import React, { useEffect, useState } from 'react'

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([]);

  async function loadManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const data = await response.json();
      const manufacturersInfoArrPromises = data.manufacturers.map(async manufacturer => {
        const url = `http://localhost:8100/api/manufacturers/${manufacturer.id}`;
        const getAManufacturerResponse = await fetch(url);
        const responseBody = await getAManufacturerResponse.json();
        return responseBody
      })

      const manufacturerInfoArray = await Promise.all(manufacturersInfoArrPromises);
      setManufacturers(manufacturerInfoArray);
    } else {
      console.error(response)
    }
  }

  useEffect(() => {
    loadManufacturers();
  }, []);

  return (
    <>
      <h3 style={{marginTop: "30px"}}>Manufacturers</h3>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>
              Name
            </th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => {
            return (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ManufacturerList;
