import React, { useEffect, useState } from 'react'


function SalespersonList(props) {
  const [salespersons, setSalespeople] = useState([]);

  const handleDeleteSalesperson = async (event) => {
    const salespersonId = event.target.value;
    const deleteUrl = `http://localhost:8090/api/salespersons/${salespersonId}/`;

    const fetchConfig = {
      method: "delete",
    };

    const response = await fetch (deleteUrl, fetchConfig);
    loadSalespeople();
  }

  async function loadSalespeople() {
    const response = await fetch('http://localhost:8090/api/salespersons/');

    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespersons);
    } else {
      console.error(response)
    }
  }

  useEffect(() => {
    loadSalespeople();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>
            Name
          </th>
          <th>
            Employee ID
          </th>
          <th>
            Delete
          </th>
        </tr>
      </thead>
      <tbody>
        {salespersons.map(salesperson => {
          return (
            <tr key={salesperson.id}>
              <td>{salesperson.name}</td>
              <td>{salesperson.employee_id}</td>
              <td>
                <button onClick={handleDeleteSalesperson} value={salesperson.id} className="btn btn-danger btn-sm">Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default SalespersonList;
