import React, { useEffect, useState } from 'react';


function SalesRecordList(props) {
  const [salesRecords, setSalesRecords] = useState([]);

  const handleDeleteSalesRecord = async (event) => {
    const salesRecordId = event.target.value;
    const deleteUrl = `http://localhost:8090/api/sales/${salesRecordId}`;

    const fetchConfig = {
      method: 'delete',
    };

    const response = await fetch(deleteUrl, fetchConfig);
    loadSalesRecords();
  };

    async function loadSalesRecords() {
        const response = await fetch('http://localhost:8090/api/sales/');


    if (response.ok) {
      const data = await response.json();


      setSalesRecords(data.salesrecords);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    loadSalesRecords();
  }, []);


  return (
    <div>
      <h1>Sales Records</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Automobile</th>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>Price</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {salesRecords.map(salesRecord => {
            return (
            <tr key={salesRecord.id}>
              <td>{salesRecord.automobile.vin}</td>
              <td>{salesRecord.salesperson.name}</td>
              <td>{salesRecord.customer.name}</td>
              <td>{salesRecord.price}</td>
              <td>
                <button
                  onClick={handleDeleteSalesRecord} value={salesRecord.id} className="btn btn-danger btn-sm">Delete</button>
              </td>
            </tr>
            );
        })}
        </tbody>
      </table>
    </div>
  );

}


export default SalesRecordList;
