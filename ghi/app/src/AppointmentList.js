import React, { useEffect, useState } from 'react'

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  const handleCancelAppointment = async (event) => {
    event.preventDefault();
    const appointmentId = event.target.value;
    const cancelUrl = `http://localhost:8080/api/appointments/${appointmentId}/cancel/`;

    const fetchConfig = {
      method: "put",
    };
    await fetch (cancelUrl, fetchConfig);

    setAppointments(appointments.filter(appointment => appointment.id !== parseInt(appointmentId)));
  }

  const handleFinishAppointment = async (event) => {
    event.preventDefault();
    const appointmentId = event.target.value;
    const finishUrl = `http://localhost:8080/api/appointments/${appointmentId}/finish/`;

    const fetchConfig = {
      method: "put",
    };

    await fetch (finishUrl, fetchConfig);
    setAppointments(appointments.filter(appointment => appointment.id !== parseInt(appointmentId)));
  }

  async function loadAppointments() {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      const appointmentInfoArrayPromises = data.appointments.map(async appointment => {
        const url = `http://localhost:8080/api/appointments/${appointment.id}`;
        const getAppointmentResponse = await fetch(url);
        const responseBody = await getAppointmentResponse.json();
        return responseBody
      })
      const appointmentInfoArray = await Promise.all(appointmentInfoArrayPromises);
      const liveAppointments = appointmentInfoArray.filter(appointment => {
        return appointment.status.name == 'created'
      })

      setAppointments(liveAppointments);
    } else {
      console.error(response)
    }
  }

  useEffect(() => {
    loadAppointments();
  }, []);

  return (
    <>
      <h3 style={{ marginTop: '30px' }}>Service Appointments</h3>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>
              Vin
            </th>
            <th>
              Is VIP?
            </th>
            <th>
              Customer
            </th>
            <th>
              Date
            </th>
            <th>
              Time
            </th>
            <th>
              Technician
            </th>
            <th>
              Reason
            </th>
            <th>
              Set Status
            </th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.isVIP ? 'Yes' : 'No'}</td>
                <td>{appointment.customer}</td>
                <td>{new Date(appointment.date_time).toDateString()}</td>
                <td>{new Date(appointment.date_time).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })}</td>
                <td>{appointment.technician.first_name}</td>
                <td>{appointment.reason}</td>
                <td>
                  <button onClick={handleCancelAppointment} value={appointment.id} className="btn btn-danger btn-sm">Cancel</button>
                  <button onClick={handleFinishAppointment} value={appointment.id} className="btn btn-success btn-sm">Finish</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );

}

export default AppointmentList;
