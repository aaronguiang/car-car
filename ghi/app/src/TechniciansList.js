import React, { useEffect, useState } from 'react'

function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);

  async function loadTechnicians() {
    const response = await fetch('http://localhost:8080/api/technicians/');
    if (response.ok) {
      const data = await response.json();
      const technicianInformationArrPromises = data.technicians.map(async technician => {
        const url = `http://localhost:8080/api/technicians/${technician.id}`;
        const getATechnicianResponse = await fetch(url);
        const responseBody = await getATechnicianResponse.json();
        return responseBody
      })

      const technicianInformationArray = await Promise.all(technicianInformationArrPromises);
      setTechnicians(technicianInformationArray);
    } else {
      console.error(response)
    }
  }

  useEffect(() => {
    loadTechnicians();
  }, []);

  return (
    <>
      <h3 style={{marginTop: "30px"}}>Technicians</h3>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>
              Employee ID
            </th>
            <th>
              First Name
            </th>
            <th>
              Last Name
            </th>
          </tr>
        </thead>
        <tbody>
          {technicians.map(technician => {
            return (
              <tr key={technician.id}>
                <td>{technician.employee_id}</td>
                <td>{technician.first_name}</td>
                <td>{technician.last_name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default TechnicianList;
