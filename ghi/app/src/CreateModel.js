import React, { useEffect, useState } from 'react'

function ModelForm() {
  const [manufacturers, setManufacturers] = useState([]);

  const [modelName, setModelName] = useState('');
  const [picture_url, setPicture] = useState('');
  const [manufacturer, setManufacturer] = useState('');

  const handleManufacturerChange = async (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const handleModelNameChange = async (event) => {
    const value = event.target.value;
    setModelName(value);
  }

  const handlePictureChange = async (event) => {
    const value = event.target.value;
    setPicture(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = modelName;
    data.manufacturer_id = manufacturer;
    data.picture_url = picture_url;

    const modelUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      setModelName('');
      setPicture('');
      setManufacturer('');
    }
  }

  const fetchManufacturersData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }

  useEffect(() => {
    fetchManufacturersData();
  }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a vehicle model</h1>
            <form onSubmit={handleSubmit} id="create-vehicle-model-form">
            <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} value={modelName} placeholder="Model name..." required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureChange} value={picture_url} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={handleManufacturerChange} value={manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                  <option>Choose a manufacturer</option>
                  {manufacturers.map(manufacturer => {
                    return (
                      <option key={manufacturer.id} value={manufacturer.id}>
                        {manufacturer.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  );
}

export default ModelForm;
