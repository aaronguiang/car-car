import React, { useEffect, useState } from 'react';


const SalesRecordForm = () => {
  const [automobile, setAutomobile] = useState('');
  const [salesperson, setSalesperson] = useState('');
  const [customer, setCustomer] = useState('');
  const [price, setPrice] = useState('');
  const [salespeople, setSalespeople] = useState([]);

  async function loadSalespeople() {
    const response = await fetch('http://localhost:8090/api/salespersons/');
    const data = await response.json()
    setSalespeople(data.salespersons)
  }

useEffect(() => {
    loadSalespeople();
}, []);

  const handleSubmit = event => {
    event.preventDefault();
    const newSalesRecord = {
      'automobile': automobile,
      'salesperson': salesperson,
      'customer': customer,
      'price': price,
    };

    const salesRecordUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(newSalesRecord),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    fetch(salesRecordUrl, fetchConfig)
      .then(response => response.json())
      .then(() => {
        setAutomobile('');
        setSalesperson('');
        setCustomer('');
        setPrice('');
      })
      .catch(e => console.error('error: ', e));
      window.location.reload();
  };

  const handleAutomobileChange = async (event) => {
    const value = event.target.value;
    setAutomobile(value);
  }


  const handleSalesPersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  }

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  }


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create Sales Record</h1>
          <form onSubmit={handleSubmit} id="create-sales-record-form">
            <div className="form-floating mb-3">
              <input value={automobile.vin} onChange={handleAutomobileChange} required type="text" name="automobile" id="automobile" className="form-control" />
              <label>Automobile Vin</label>
            </div>
            <div className="form-floating mb-3">
              <input value={salesperson.id} onChange={handleSalesPersonChange} required type="text" name="salesperson" id="salesperson" className="form-control" />
              <label>Salesperson ID</label>
            </div>
            <div className="form-floating mb-3">
              <input value={customer.id} onChange={handleCustomerChange} required type="text" name="customer" id="customer" className="form-control" />
              <label>Customer ID</label>
            </div>
            <div className="form-floating mb-3">
              <input value={price} onChange={handlePriceChange} required type="text" name="price" id="price" className="form-control" />
              <label>Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesRecordForm;
