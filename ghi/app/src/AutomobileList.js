import React, { useEffect, useState } from 'react'

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);

  async function loadAutomobiles() {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
      const data = await response.json();
      const automobileInformationArrPromises = data.autos.map(async automobile => {
        const url = `http://localhost:8100/api/automobiles/${automobile.vin}`;
        const getAnAutomobileResponse = await fetch(url);
        const responseBody = await getAnAutomobileResponse.json();
        return responseBody
      })

      const automobileInformationArray = await Promise.all(automobileInformationArrPromises);
      setAutomobiles(automobileInformationArray);
    } else {
      console.error(response)
    }
  }

  useEffect(() => {
    loadAutomobiles();
  }, []);

  return (
    <>
      <h3 style={{marginTop: "30px"}}>Automobiles</h3>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>
              Vin
            </th>
            <th>
              Color
            </th>
            <th>
              Year
            </th>
            <th>
              Model
            </th>
            <th>
              Manufacturer
            </th>
            <th>
              Sold
            </th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(automobile => {
            return (
              <tr key={automobile.id}>
                <td>{automobile.vin}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>
                <td>{automobile.sold ? 'Yes': 'No'}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default AutomobileList;
