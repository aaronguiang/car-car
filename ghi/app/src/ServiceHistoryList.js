import React, { useEffect, useState } from 'react'
import App from './App';

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [vin, setVin] = useState('');

  async function handleSearchChange(event) {
    setVin(event.target.value)
  }

  async function handleSubmit(event) {
    event.preventDefault()

    setAppointments(appointments.filter(appointment => appointment.vin == vin))
  }

  async function loadAppointments() {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      const appointmentInfoArrayPromises = data.appointments.map(async appointment => {
        const url = `http://localhost:8080/api/appointments/${appointment.id}`;
        const getAppointmentResponse = await fetch(url);
        const responseBody = await getAppointmentResponse.json();
        return responseBody
      })
      const appointmentInfoArray = await Promise.all(appointmentInfoArrayPromises);

      setAppointments(appointmentInfoArray);
    } else {
      console.error(response)
    }
  }

  useEffect(() => {
    loadAppointments();
  }, []);

  return (
    <>
      <h3 style={{ marginTop: '30px' }}>Service History</h3>
      <form onSubmit={handleSubmit} id="create-technician-form">
        <div className="mb-3">
          <input style={{ width: '90%', marginRight: '10px' }} onChange={handleSearchChange} value={vin} placeholder="Search by VIN..." required type="text" name="vin" id="vin"/>
          <button className="btn btn-primary btn-sm">Search</button>
        </div>
      </form>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>
              Vin
            </th>
            <th>
              Is VIP?
            </th>
            <th>
              Customer
            </th>
            <th>
              Date
            </th>
            <th>
              Time
            </th>
            <th>
              Technician
            </th>
            <th>
              Reason
            </th>
            <th>
              Status
            </th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.isVIP ? 'Yes' : 'No'}</td>
                <td>{appointment.customer}</td>
                <td>{new Date(appointment.date_time).toDateString()}</td>
                <td>{new Date(appointment.date_time).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })}</td>
                <td>{appointment.technician.first_name}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );

}

export default ServiceHistory;
