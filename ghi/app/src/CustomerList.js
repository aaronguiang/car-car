import React, { useEffect, useState } from 'react'


function CustomerList(props) {
  const [customers, setCustomers] = useState([]);

  const handleDeleteCustomer = async (event) => {
    const customerId = event.target.value;
    const deleteUrl = `http://localhost:8090/api/customers/${customerId}/`;

    const fetchConfig = {
      method: "delete",
    };

    const response = await fetch (deleteUrl, fetchConfig);
    loadCustomers();
  }

  async function loadCustomers() {
    const response = await fetch('http://localhost:8090/api/customers/');

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);

    } else {
      console.error(response)
    }
  }

  useEffect(() => {
    loadCustomers();
  }, []);

  return (
    <div>
    <h1>Customer List</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>
            Name
          </th>
          <th>
            Address
          </th>
          <th>
            Phone Number
          </th>
          <th>
            Delete
          </th>
        </tr>
      </thead>
      <tbody>
        {customers.map(customer => {
          return (
            <tr key={customer.id}>
              <td>{customer.name}</td>
              <td>{customer.address}</td>
              <td>{customer.phone_number}</td>
              <td>
                <button onClick={handleDeleteCustomer} value={customer.id} className="btn btn-danger btn-sm">Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </div>
  );

}

export default CustomerList;
