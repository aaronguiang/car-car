from common.json import ModelEncoder
from .models import Technician, Status, Appointment


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
    ]


class StatusEncoder(ModelEncoder):
    model = Status
    properties = [
        "name",
    ]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
    ]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "customer",
        "vin",
        "technician",
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
        "status": StatusEncoder(),
    }

    def get_extra_data(self, o):
        return { "isVIP": o.isVIP }
