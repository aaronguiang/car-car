from django.shortcuts import render
from service_rest.encoders import TechnicianListEncoder, TechnicianDetailEncoder, AppointmentListEncoder, AppointmentDetailEncoder, StatusEncoder
from .models import Technician, AutomobileVO, Appointment, Status
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods({"DELETE", "GET", "PUT"})
def show_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        Technician.objects.filter(id=id).update(**content)
        technician = Technician.objects.get(id=id)

        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician"},
                status=400,
            )
        try:
            status = Status.objects.get(name="created")
            content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Status"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def show_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        try:
            AutomobileVO.objects.get(vin=appointment.vin)
            appointment.isVIP = True
        except AutomobileVO.DoesNotExist:
            appointment.isVIP = False

        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Appointment.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    content = {}
    status = Status.objects.get(name="finished")

    content["status"] = status
    Appointment.objects.filter(id=id).update(**content)
    appointment = Appointment.objects.get(id=id)

    try:
        AutomobileVO.objects.get(vin=appointment.vin)
        appointment.isVIP = True
    except AutomobileVO.DoesNotExist:
        appointment.isVIP = False

    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    content = {}
    status = Status.objects.get(name="canceled")

    content["status"] = status
    Appointment.objects.filter(id=id).update(**content)
    appointment = Appointment.objects.get(id=id)

    try:
        AutomobileVO.objects.get(vin=appointment.vin)
        appointment.isVIP = True
    except AutomobileVO.DoesNotExist:
        appointment.isVIP = False

    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )
